# bucket

### What is a *Bucket*?

Well, a *bucket* can be defined as a watertight, vertical cylinder or truncated cone or square, with an open top and a flat bottom.

Metaphorically, in the Greek language, **all lost bets and all fails**, inescapably ***'end up in the bucket'***.

Bucket is also a **betting card game**.

A card game, in this case, written in javascript, where you can bet whether your card is between the two other cards.

This is how it is called in Greece anyway.

Many variations with other names (*Acey Deucey, In-Between, Yablon, Red Dog, Sheets, Between the Sheets, Maverick* etc) do exist in other places.



The goal is to amount as much money as possible.

![4.png](png/4.png)

## INSTRUCTIONS

* Download zip file pressing the download button above.

* Unzip

* Double-click on index.html file, to open the game in a browser tab.

---
Linux users, in order to have a better experience are encouraged to use [Web Apps](https://github.com/linuxmint/webapp-manager).

---
## LET'S WIN SOME EASY MONEY! (or get poor tryin')

As soon as you run the program, you will be prompted to select the type of  game.

You can select either

1. a game of ten hands (amount as much money as possible in 10 hands)

2. to **Go Deep** (no hand limit, you can play until you lose all your money)

![0.png](png/00.png)


* __Click on the Deal button__ to start playing.

![0.png](png/0.png)

* __Slide or Scroll on the Bet button__ in order to adjust your bet up or down,


* __OR click on the Deal button again__ for another deal.

![1.png](png/1.png)

* __Click on the Bet button__ to place your bet.

![2.png](png/2.png)

* After 10 hands (or if you **Go Deep**, when you run out of money, you will be shown your performance, and asked if you would like to play again.

![3.png](png/3.png)

---

__NOTE:__ There is no use counting the cards, as the cards are independently generated in each hand.

---

## Settings

*   By clicking on the small __settings 🛠 button at the bottom right of the screen__ the user can set the appearance of the game according to his liking.

![preferences.png](png/preferences.png)

The user can set

*   the background color
* the deck of cards
* the back image of the deck
* Key that hides / shows again the game (boss key 😉 )
* The initial sum
* The currency ($,€,￥,￡ etc)
* The currency format (coin symbol before or after the amount)
* the ante (the amount the player is charged in order to be dealt. )
* the ante raise ( the number of deals after which the ante is raised: every 10, every 20 and so on)
* Show Trophies On / Off
* Sound On / Off
*  Save / Load settings in / to a file, for easy & quick access from the file system.

Clicking on the __Done__ button will bring the user back to the game.


---

When you win a bold bet , or a big amount, the game rewards you with a trophy!

 ![x1000](png/x1000.png)

---
  Special thanks to the members of
[https://forums.linuxmint.com](https://forums.linuxmint.com)

who, with their feedback and input, made shaping up this game so much fun!

---

**ENJOY!**
